define(function(require, exports, module) {
  'use strict';

  const oop = require('../lib/oop');
  const TextHighlightRules = require('./text_highlight_rules').TextHighlightRules;

  const EnoHighlightRules = function() {
    this.$rules = {
      start: [
        {
          // > [comment]
          regex: '>.*',
          token: 'comment.line.eno'
        },
        {
          // -- embed
          next: 'embed',
          onMatch: function(value, currentState, state) {
            const tokens = value.split(this.splitRegex);
            state.unshift(this.next, tokens[1].length.toString() + tokens[3]);

            return [
              { type: 'punctuation.definition.embed.begin.eno', value: tokens[1] },
              { type: 'text', value: tokens[2] },
              { type: 'variable.other.name.embed.eno', value: tokens[3] }
            ];
          },
          regex: /(-{2,})(?!-)(\s*)(\S[^\n]*?)(?:\s*$)/
        },
        {
          // - (item)
          regex: /(-)(\s*)(.+?)?(?:\s*$)/,
          token: [
            'punctuation.definition.item.eno',
            'text',
            'string.unquoted.eno'
          ]
        },
        {
          // # section
          regex: /(#{1,})(?!#)(\s*)([^\s<`][^\n<]*?)(?:\s*$)/,
          token: [
            'punctuation.definition.section.eno',
            'text',
            'entity.name.section.eno'
          ]
        },
        {
          // flag
          regex: /([^\s>\-#=:`][^\n=:]*?)(?:\s*)$/,
          token: [
            'variable.other.name.flag.eno'
          ]
        },
        {
          // field: (value)
          regex: /([^\s>\-#=:`][^\n=:]*?)(\s*)(:)(\s*)(\S.*?)?(?:\s*)$/,
          token: [
           'variable.other.name.field.eno',
           'text',
           'punctuation.separator.field.eno',
           'text',
           'string.unquoted.value.eno'
          ]
        },
        {
          // attribute = (value)
          regex: /([^\s>\-#=:`][^\n=:]*?)(\s*)(=)(\s*)(\S.*?)?(?:\s*)$/,
          token: [
            'variable.other.name.attribute.eno',
            'text',
            'punctuation.separator.attribute.eno',
            'text',
            'string.unquoted.value.eno'
          ]
        },
        {
          // `flag`
          regex: /(`+)(\s*)((?:(?!\1).)+)(\s*)(\1)(?:\s*)$/,
          token: [
            'punctuation.definition.key.escape.begin.eno',
            'text',
            'variable.other.name.flag.eno',
            'text',
            'punctuation.definition.key.escape.end.eno'
          ]
        },
        {
          // `field`: (value)
          regex: /(`+)(\s*)((?:(?!\1).)+)(\s*)(\1)(\s*)(:)(\s*)(\S.*?)?(?:\s*)$/,
          token: [
            'punctuation.definition.key.escape.begin.eno',
            'text',
            'variable.other.name.field.eno',
            'text',
            'punctuation.definition.key.escape.end.eno',
            'text',
            'punctuation.separator.field.eno',
            'text',
            'string.unquoted.value.eno'
          ]
        },
        {
          // `attribute` = (value)
          regex: /(`+)(\s*)((?:(?!\1).)+)(\s*)(\1)(\s*)(=)(\s*)(\S.*?)?(?:\s*)$/,
          token: [
            'punctuation.definition.key.escape.begin.eno',
            'text',
            'variable.other.name.attribute.eno',
            'text',
            'punctuation.definition.key.escape.end.eno',
            'text',
            'punctuation.separator.attribute.eno',
            'text',
            'string.unquoted.value.eno'
          ]
        },
        {
          regex: /\S/,
          token: 'invalid.illegal.eno'
        }
      ],
      embed: [
        {
          next: 'start',
          onMatch: function(value, currentState, state) {
            const tokens = value.split(this.splitRegex);

            if(tokens[1].length.toString() + tokens[3] !== state[1])
              return 'string.unquoted.eno';

            state.shift();
            state.shift();

            return [
              { type: 'punctuation.definition.embed.end.eno', value: tokens[1] },
              { type: 'text', value: tokens[2] },
              { type: 'variable.other.name.embed.eno', value: tokens[3] }
            ];
          },
          regex: /(-{2,})(?!-)(\s*)(\S[^\n]*?)(?:\s*$)/
        },
        {
          defaultToken: 'string.unquoted.eno'
        }
      ]
    };

    this.normalizeRules();
  };

  EnoHighlightRules.metaData = {
    fileTypes: ['eno'],
    name: 'Eno',
    scopeName: 'text.eno'
  };

  oop.inherits(EnoHighlightRules, TextHighlightRules);

  exports.EnoHighlightRules = EnoHighlightRules;
});
