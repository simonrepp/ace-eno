# ace-eno

Eno language support for the [Ace](https://ace.c9.io/) editor

## Using the included prebuilt ace custom build

`builds/` contains the same content that
[ace-builds](https://www.npmjs.com/package/ace-builds) provides (currently with
version 1.32.3 of ace). Refer to their instructions, but using the files from
here instead, if you want to use this pre-built custom ace build with eno
support.

## Building

To generate your own custom ace editor build with eno support follow these steps:

- Clone the official ace repository at https://github.com/ajaxorg/ace
- Copy `mode/eno.js` and `mode/eno_highlight_rules.js` from this repository into the `lib/ace/mode/` folder in your cloned ace repository.
- Copy `snippets/eno.js` and `snippets/eno.snippets` from this repository into the `lib/ace/snippets/` folder in your cloned ace repository.
- Run `npm install` inside the cloned ace respository
- Run something like `node Makefile.dryice.js full --target ../ace-builds` inside the cloned ace repository (consult their `README.md` for detailed instructions)
- If you ran exactly the command above, the output in `../ace-builds` contains the same build structure as you find in this repository in `builds/`
